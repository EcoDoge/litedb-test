﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteDB
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new LiteDatabase(@"./lite.db"))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Book>("customers");

                // Create your new customer instance
                var book = new Book
                {
                    Title = "John Doe"
                };

                // Insert new customer document (Id will be auto-incremented)
                col.Insert(book);

                // Update a document inside a collection
                book.Title = "Joana Doe";

                col.Update(book);

                // Index document using document Name property
                col.EnsureIndex(x => x.Title);

                // Use LINQ to query documents
                try
                {
                    var results = col.Find(x => x.Title.StartsWith("Jo")).First();
                    Console.WriteLine(results.Title);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
